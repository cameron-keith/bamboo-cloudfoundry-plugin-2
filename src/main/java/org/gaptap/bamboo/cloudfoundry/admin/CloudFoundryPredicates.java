/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.admin;

import static org.gaptap.bamboo.cloudfoundry.tasks.config.BaseCloudFoundryTaskConfigurator.TARGET_ID;

import javax.annotation.Nullable;

import com.atlassian.bamboo.build.Job;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.task.TaskIdentifier;
import com.atlassian.bamboo.task.TaskResult;
import org.gaptap.bamboo.cloudfoundry.PluginProperties;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Collections2;

/**
 * @author David Ehringer
 * 
 */
public class CloudFoundryPredicates {

	// TODO this is not longer admin specific and should move to a different
	// package
	
	// TODO look into ways to better compose predicates

	private static final String CLOUD_FOUNDRY_TASK_PREFIX = PluginProperties.getPluginKey();

	public static Predicate<Job> isJobWithCloudFoundryTask() {
		return new Predicate<Job>() {
			@Override
			public boolean apply(@Nullable Job job) {
				if (Collections2.filter(job.getBuildDefinition().getTaskDefinitions(), isCloudFoundryTaskDefinition())
						.size() > 0) {
					return true;
				}
				return false;
			}
		};
	}

	// NOTE: isCloudFoundryTask will handle this also
	public static Predicate<TaskDefinition> isCloudFoundryTaskDefinition() {
		return new Predicate<TaskDefinition>() {
			@Override
			public boolean apply(@Nullable TaskDefinition taskDefinition) {
				return taskDefinition.getPluginKey().startsWith(CLOUD_FOUNDRY_TASK_PREFIX);
			}
		};
	}

	public static Predicate<TaskIdentifier> isCloudFoundryTask() {
		return new Predicate<TaskIdentifier>() {
			@Override
			public boolean apply(@Nullable TaskIdentifier taskIdentifier) {
				return taskIdentifier.getPluginKey().startsWith(CLOUD_FOUNDRY_TASK_PREFIX);
			}
		};
	}

	public static Predicate<TaskIdentifier> isCloudFoundryPushTask() {
		return new Predicate<TaskIdentifier>() {
			@Override
			public boolean apply(@Nullable TaskIdentifier taskIdentifier) {
				String pluginKey = taskIdentifier.getPluginKey();
				return isCloudFoundryTask().apply(taskIdentifier)
						&& pluginKey.endsWith(PluginProperties.getPushTaskKey());
			}
		};
	}

    public static Predicate<TaskIdentifier> isCloudFoundryApplicationTask() {
        return new Predicate<TaskIdentifier>() {
            @Override
            public boolean apply(@Nullable TaskIdentifier taskIdentifier) {
                String pluginKey = taskIdentifier.getPluginKey();
                return isCloudFoundryTask().apply(taskIdentifier)
                        && pluginKey.endsWith(PluginProperties.getApplicationTaskKey());
            }
        };
    }

    public static Predicate<TaskIdentifier> isCloudFoundryServiceTask() {
        return new Predicate<TaskIdentifier>() {
            @Override
            public boolean apply(@Nullable TaskIdentifier taskIdentifier) {
                String pluginKey = taskIdentifier.getPluginKey();
                return isCloudFoundryTask().apply(taskIdentifier)
                        && pluginKey.endsWith(PluginProperties.getServiceTaskKey());
            }
        };
    }

	public static Predicate<TaskResult> isCloudFoundryTaskResult() {
		return new Predicate<TaskResult>() {
			@Override
			public boolean apply(@Nullable TaskResult taskResult) {
				return isCloudFoundryTask().apply(taskResult.getTaskIdentifier());
			}
		};
	}

	public static Predicate<TaskDefinition> isReferencing(Target target) {
		return Predicates.and(isCloudFoundryTaskDefinition(), new ReferencesTarget<TaskDefinition>(target));
	}

	public static Predicate<Target> targetsUsing(final Proxy proxy) {
		return new Predicate<Target>() {
			@Override
			public boolean apply(@Nullable Target target) {
				return target.getProxy() != null && target.getProxy().getID() == proxy.getID();
			}
		};
	}

	public static Predicate<Target> targetsUsing(final Credentials credentials) {
		return new Predicate<Target>() {
			@Override
			public boolean apply(@Nullable Target target) {
				return target.getCredentials() != null && target.getCredentials().getID() == credentials.getID();
			}
		};
	}

	private static class ReferencesTarget<TASKDEF extends TaskDefinition> implements Predicate<TASKDEF> {

		private final Target target;

		public ReferencesTarget(Target target) {
			this.target = target;
		}

		@Override
		public boolean apply(@Nullable TASKDEF taskDefinition) {
			String taskTargetId = taskDefinition.getConfiguration().get(TARGET_ID);
			if (String.valueOf(target.getID()).equals(taskTargetId)) {
				return true;
			}
			return false;
		}
	}
}
