/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.cli;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @author David Ehringer
 */
class TableBuilder {

    List<String[]> rows = new LinkedList<>();

    public void addRow(String... cols)
    {
        rows.add(cols);
    }

    private int[] colWidths() {
        int cols = -1;

        for(String[] row : rows) {
            cols = Math.max(cols, row.length);
        }

        int[] widths = new int[cols];

        for(String[] row : rows) {
            for(int colNum = 0; colNum < row.length; colNum++) {
                widths[colNum] = Math.max(widths[colNum], StringUtils.length(row[colNum]));
            }
        }

        return widths;
    }

    public List<String> getTable(){
        List<String> outputRows = new ArrayList<>();

        int[] columnWidths = colWidths();
        for(String[] row : rows) {
            StringBuilder content = new StringBuilder();
            for(int columnNumber = 0; columnNumber < row.length; columnNumber++) {
                content.append(StringUtils.rightPad(StringUtils.defaultString(row[columnNumber]), columnWidths[columnNumber]));
                content.append("   ");
            }
            outputRows.add(content.toString());
        }
        return outputRows;
    }
}
