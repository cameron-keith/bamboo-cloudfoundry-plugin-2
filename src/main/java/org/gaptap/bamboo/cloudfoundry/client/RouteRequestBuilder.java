/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.client;


import org.cloudfoundry.operations.routes.MapRouteRequest;
import org.cloudfoundry.operations.routes.UnmapRouteRequest;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RouteRequestBuilder {

    private static final Pattern ROUTE_PATTERN = Pattern.compile("([A-Za-z0-9\\-\\_\\.]+)(:[0-9]+)?(.*)$");
    private static final Pattern HOST_DOMAIN_PATTERN = Pattern.compile("([A-Za-z0-9\\-\\_]+)\\.([A-Za-z0-9\\-\\_\\.]+)");

    public static MapRouteRequest buildMapRouteRequest(String appName, String uri) {
        Matcher matcher = ROUTE_PATTERN.matcher(uri);
        if(matcher.matches()){
            String path = null;
            if(matcher.groupCount() >= 3 && matcher.group(3).length() >= 2) {
                path = matcher.group(3);
            }

            Matcher hostDomainMatcher = HOST_DOMAIN_PATTERN.matcher(matcher.group(1));
            if(hostDomainMatcher.matches()) {
                return MapRouteRequest.builder()
                        .applicationName(appName)
                        .host(hostDomainMatcher.group(1))
                        .domain(hostDomainMatcher.group(2))
                        .path(path)
                        .build();
            }
        }
        throw new IllegalArgumentException(uri + " is not a valid route.");
    }

    public static UnmapRouteRequest buildUnmapRouteRequest(String appName, String uri){
        MapRouteRequest mapRouteRequest = buildMapRouteRequest(appName, uri);
        return UnmapRouteRequest.builder()
                .applicationName(appName)
                .host(mapRouteRequest.getHost())
                .domain(mapRouteRequest.getDomain())
                .path(mapRouteRequest.getPath())
                .build();
    }

    public static String toString(MapRouteRequest request) {
        StringBuilder result = new StringBuilder();
        if(request.getHost() != null){
            result.append(request.getHost());
            result.append(".");
        }
        result.append(request.getDomain());
        if(request.getPath() != null){
            result.append(request.getPath());
        }
        return result.toString();
    }

    public static String toString(UnmapRouteRequest request) {
        StringBuilder result = new StringBuilder();
        if(request.getHost() != null){
            result.append(request.getHost());
            result.append(".");
        }
        result.append(request.getDomain());
        if(request.getPath() != null){
            result.append(request.getPath());
        }
        return result.toString();
    }

    public static List<MapRouteRequest> buildMapRouteRequests(ApplicationConfiguration applicationConfiguration) {
        List<MapRouteRequest> requests = new ArrayList<>();
        requests.addAll(mapDomainsAndHosts(applicationConfiguration));
        requests.addAll(mapRoutes(applicationConfiguration));
        return requests;
    }

    private static List<MapRouteRequest> mapDomainsAndHosts(ApplicationConfiguration applicationConfiguration) {
        List<MapRouteRequest> requests = new ArrayList<>();
        for (String domain : applicationConfiguration.domains()) {
            if (!applicationConfiguration.noHostname()) {
                if (applicationConfiguration.hosts().isEmpty()) {
                    MapRouteRequest request = MapRouteRequest.builder()
                            .applicationName(applicationConfiguration.name())
                            .domain(domain)
                            .host(applicationConfiguration.name())
                            .build();
                    requests.add(request);
                } else {
                    for (String host : applicationConfiguration.hosts()) {
                        MapRouteRequest request = MapRouteRequest.builder()
                                .applicationName(applicationConfiguration.name())
                                .domain(domain)
                                .host(host)
                                .build();
                        requests.add(request);
                    }
                }
            } else {
                MapRouteRequest request = MapRouteRequest.builder()
                        .applicationName(applicationConfiguration.name())
                        .domain(domain)
                        .build();
                requests.add(request);
            }
        }
        return requests;
    }

    private static List<MapRouteRequest> mapRoutes(ApplicationConfiguration applicationConfiguration) {
        List<MapRouteRequest> requests = new ArrayList<>();
        applicationConfiguration
                .routes()
                .forEach(route -> requests.add(buildMapRouteRequest(applicationConfiguration.name(), route)));
        return requests;
    }
}
