/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.tasks;


import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.security.EncryptionService;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.atlassian.bamboo.task.CommonTaskType;
import com.atlassian.bamboo.task.TaskContext;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.gaptap.bamboo.cloudfoundry.client.CloudFoundryService;
import org.gaptap.bamboo.cloudfoundry.client.CloudFoundryServiceFactory;
import org.gaptap.bamboo.cloudfoundry.client.ConnectionParameters;
import org.gaptap.bamboo.cloudfoundry.client.DefaultCloudFoundryServiceFactory;
import org.jetbrains.annotations.NotNull;
import reactor.core.publisher.Mono;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

import static org.gaptap.bamboo.cloudfoundry.tasks.config.BaseCloudFoundryTaskConfigurator.ORGANIZATION;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.BaseCloudFoundryTaskConfigurator.SPACE;
import static org.gaptap.bamboo.cloudfoundry.tasks.dataprovider.TargetTaskDataProvider.DISABLE_FOR_BUILD_PLANS;
import static org.gaptap.bamboo.cloudfoundry.tasks.dataprovider.TargetTaskDataProvider.IS_PASSWORD_ENCRYPTED;
import static org.gaptap.bamboo.cloudfoundry.tasks.dataprovider.TargetTaskDataProvider.PASSWORD;
import static org.gaptap.bamboo.cloudfoundry.tasks.dataprovider.TargetTaskDataProvider.PROXY_HOST;
import static org.gaptap.bamboo.cloudfoundry.tasks.dataprovider.TargetTaskDataProvider.PROXY_PORT;
import static org.gaptap.bamboo.cloudfoundry.tasks.dataprovider.TargetTaskDataProvider.TARGET_URL;
import static org.gaptap.bamboo.cloudfoundry.tasks.dataprovider.TargetTaskDataProvider.TRUST_SELF_SIGNED_CERTS;
import static org.gaptap.bamboo.cloudfoundry.tasks.dataprovider.TargetTaskDataProvider.USERNAME;

/**
 * @author David Ehringer
 */
public abstract class AbstractCloudFoundryTask implements CommonTaskType {

	private static final org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(AbstractCloudFoundryTask.class);

	private CloudFoundryServiceFactory cloudFoundryServiceFactory;

	public AbstractCloudFoundryTask(EncryptionService encryptionService){
		this.cloudFoundryServiceFactory = new DefaultCloudFoundryServiceFactory(encryptionService);
	}

	/**
	 * This is primarily for testing purposes. Optimally, we would like to inject the CloudFoundryServiceFactory into
	 * the constructor rather than EncryptionService. But that only works on local agents and not remote agents.
	 */
	void setCloudFoundryServiceFactory(CloudFoundryServiceFactory cloudFoundryServiceFactory){
		this.cloudFoundryServiceFactory = cloudFoundryServiceFactory;
	}
    
	@Override
    @NotNull
    public final TaskResult execute(@NotNull CommonTaskContext taskContext)
            throws TaskException {
        BuildLogger buildLogger = taskContext.getBuildLogger();
	    boolean disableForBuildPlans = Boolean.valueOf(taskContext.getRuntimeTaskContext().get(DISABLE_FOR_BUILD_PLANS));
        if(disableForBuildPlans && (taskContext instanceof TaskContext)){
            String targetUrl = taskContext.getRuntimeTaskContext().get(TARGET_URL);
            buildLogger.addErrorLogEntry("Cannot execute Task. Target " + targetUrl + " cannot be used within a Build Plan.");
            return TaskResultBuilder.newBuilder(taskContext).failedWithError().build(); 
        }
		try {
			return doExecute(taskContext);
		} catch (Exception e){
			LOG.error("Task execution failed due to unknown exception", e);
			buildLogger.addErrorLogEntry("Task execution failed due to: " + e.getMessage());
			return TaskResultBuilder.newBuilder(taskContext).failedWithError().build();
		}
    }
	
    protected abstract TaskResult doExecute(CommonTaskContext taskContext) throws TaskException;
    
    protected String getTarget(CommonTaskContext taskContext) {
		return taskContext.getRuntimeTaskContext().get(TARGET_URL);
	}

	protected String getOrganization(CommonTaskContext taskContext){
		return taskContext.getConfigurationMap().get(ORGANIZATION);
	}

	protected String getSpace(CommonTaskContext taskContext){
		return taskContext.getConfigurationMap().get(SPACE);
	}

	protected CloudFoundryService getCloudFoundryService(CommonTaskContext taskContext) throws TaskException {
		BuildLoggerFacade logger = new BuildLoggerFacade(taskContext.getBuildLogger());

		logger.info("Connecting to Cloud Foundry Cloud Controller API at " + getTarget(taskContext));

        String org = getOrganization(taskContext);
        String space = getSpace(taskContext);

		return cloudFoundryServiceFactory.getCloudFoundryService(createConnectionParameters(taskContext), org, space, logger);
	}

	private ConnectionParameters createConnectionParameters(CommonTaskContext taskContext) {
		String target = getTarget(taskContext);
		boolean trustSelfSignedCerts = Boolean.valueOf(taskContext.getRuntimeTaskContext().get(TRUST_SELF_SIGNED_CERTS));
		String username = taskContext.getRuntimeTaskContext().get(USERNAME);
		String password = taskContext.getRuntimeTaskContext().get(PASSWORD);
		boolean isPasswordEncrypted = true;
		if(!StringUtils.isBlank(taskContext.getRuntimeTaskContext().get(IS_PASSWORD_ENCRYPTED))){
			isPasswordEncrypted = Boolean.valueOf(taskContext.getRuntimeTaskContext().get(IS_PASSWORD_ENCRYPTED));
		}

		String proxyHost = taskContext.getRuntimeTaskContext().get(PROXY_HOST);
		Integer proxyPort = null;
		if (taskContext.getRuntimeTaskContext().get(PROXY_PORT) != null) {
			proxyPort = Integer.parseInt(taskContext.getRuntimeTaskContext().get(PROXY_PORT));
		}

		return ConnectionParameters.builder()
				.targetUrl(target)
				.isTrustSelfSignedCerts(trustSelfSignedCerts)
				.username(username)
				.password(password)
				.isPasswordEncrypted(isPasswordEncrypted)
				.proxyHost(proxyHost)
				.proxyPort(proxyPort)
				.build();
	}

	protected String getLoginContext(CommonTaskContext taskContext) {
		StringBuilder message = new StringBuilder();
		message.append("in org ");
		message.append(getOrganization(taskContext));
		message.append(" / space ");
		message.append(getSpace(taskContext));
		message.append(" as ");
		message.append(taskContext.getRuntimeTaskContext().get(USERNAME));
		message.append("...");
		return message.toString();
	}

	protected void doSubscribe(Mono<Void> mono, String errorLogEntry,  BuildLogger buildLogger,
							 TaskResultBuilder taskResultBuilder) throws InterruptedException {
		CountDownLatch latch = new CountDownLatch(1);
		mono.subscribe(aVoid -> {
				},
				throwable -> {
					buildLogger.addErrorLogEntry(errorLogEntry + ": " + throwable.getMessage());
					taskResultBuilder.failedWithError();
					latch.countDown();
				},
				latch::countDown
		);
		latch.await();
	}

	protected Map<String, Object> createCredentialsFromJson(String json, BuildLogger buildLogger) throws TaskException {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.readValue(json, new TypeReference<HashMap<String, Object>>() {
			});
		} catch (JsonParseException e) {
			buildLogger.addErrorLogEntry("Invalid JSON", e);
			throw new TaskException("Invalid JSON");
		} catch (JsonMappingException e) {
			buildLogger.addErrorLogEntry("Invalid JSON", e);
			throw new TaskException("Invalid JSON");
		} catch (IOException e) {
			buildLogger.addErrorLogEntry("Invalid JSON", e);
			throw new TaskException("Invalid JSON");
		}
	}

	protected Map<String, Object> createCredentialsFromFile(File file, BuildLogger buildLogger) throws TaskException {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.readValue(new FileInputStream(file), new TypeReference<HashMap<String, Object>>() {
			});
		} catch (JsonParseException e) {
			buildLogger.addErrorLogEntry("Invalid JSON", e);
			throw new TaskException("Invalid JSON");
		} catch (JsonMappingException e) {
			buildLogger.addErrorLogEntry("Invalid JSON", e);
			throw new TaskException("Invalid JSON");
		} catch (IOException e) {
			buildLogger.addErrorLogEntry("Unable to load JSON from file.", e);
			throw new TaskException("Unable to load JSON from file.");
		}
	}
}
