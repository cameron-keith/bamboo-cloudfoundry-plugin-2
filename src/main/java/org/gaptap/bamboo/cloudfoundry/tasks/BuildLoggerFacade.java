/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.tasks;

import com.atlassian.bamboo.build.logger.BuildLogger;
import org.gaptap.bamboo.cloudfoundry.client.Logger;

/**
 * @author David Ehringer
 * 
 */
public class BuildLoggerFacade implements Logger {

	private final BuildLogger buildLogger;

	public BuildLoggerFacade(BuildLogger buildLogger) {
		this.buildLogger = buildLogger;
	}

	@Override
	public void info(String message) {
		buildLogger.addBuildLogEntry(message);

	}

	@Override
	public void warn(String message) {
		buildLogger.addBuildLogEntry(message);
	}

	@Override
	public void error(String message) {
		buildLogger.addErrorLogEntry(message);
	}

}
