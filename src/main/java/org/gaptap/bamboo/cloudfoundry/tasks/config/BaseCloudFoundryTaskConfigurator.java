/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.tasks.config;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.security.EncryptionService;
import com.atlassian.bamboo.task.AbstractTaskConfigurator;
import com.atlassian.bamboo.task.TaskConfiguratorHelper;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.atlassian.struts.TextProvider;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.gaptap.bamboo.cloudfoundry.admin.CloudFoundryAdminService;
import org.gaptap.bamboo.cloudfoundry.admin.Target;
import org.gaptap.bamboo.cloudfoundry.client.CloudFoundryService;
import org.gaptap.bamboo.cloudfoundry.client.CloudFoundryServiceException;
import org.gaptap.bamboo.cloudfoundry.client.CloudFoundryServiceFactory;
import org.gaptap.bamboo.cloudfoundry.client.ConnectionParameters;
import org.gaptap.bamboo.cloudfoundry.client.InvalidUrlException;
import org.gaptap.bamboo.cloudfoundry.client.Log4jLogger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author David Ehringer
 * 
 */
public class BaseCloudFoundryTaskConfigurator extends AbstractTaskConfigurator {

    public static final String ENVIRONMENT_CONFIG_OPTION = "cf_environment_config_option";
    public static final String ENVIRONMENT_CONFIG_OPTIONS = "cf_environment_config_options";
    public static final String ENVIRONMENT_CONFIG_OPTION_SHARED = "shared";
    public static final String ENVIRONMENT_CONFIG_OPTION_TASK = "task_level";

    public static final String ENVIRONMENT_URL = "cf_environment_url";
    public static final String ENVIRONMENT_USERNAME = "cf_environment_username";
    static final String ENVIRONMENT_PASSWORD = "cf_environment_password";
    public static final String ENVIRONMENT_PASSWORD_ENCRYPTED = "cf_environment_password_enc";
    public static final String ENVIRONMENT_IS_PASSWORD_ENCRYPTED = "cf_environment_is_password_encrypted";
    public static final String ENVIRONMENT_TRUST_CERTS = "cf_environment_trustSelfSignedCerts";
    public static final String ENVIRONMENT_PROXY_HOST = "cf_environment_proxy_host";
    public static final String ENVIRONMENT_PROXY_PORT = "cf_environment_proxy_port";

    public static final String TARGETS = "cf_targets";
    public static final String BUILD_PLAN_TARGETS = "cf_build_targets";
    public static final String TARGET_ID = "cf_targetId";
    public static final String ORGANIZATION = "cf_org";
    public static final String SPACE = "cf_space";

    static final String PASSWORD_CHANGE = "passwordChange";

    private static final Pattern BAMBOO_VARIABLE_PATTERN = Pattern.compile(".*\\$\\{.*\\}.*");

    protected final CloudFoundryAdminService adminService;
    protected final TextProvider textProvider;
    protected final EncryptionService encryptionService;
    protected final CloudFoundryServiceFactory cloudFoundryServiceFactory;

    private static final List<String> FIELDS_TO_COPY = ImmutableList.of(TARGET_ID, ORGANIZATION, SPACE, ENVIRONMENT_CONFIG_OPTION,
            ENVIRONMENT_URL, ENVIRONMENT_USERNAME, ENVIRONMENT_PASSWORD_ENCRYPTED, ENVIRONMENT_TRUST_CERTS, ENVIRONMENT_PROXY_HOST, ENVIRONMENT_PROXY_PORT);

    public BaseCloudFoundryTaskConfigurator(CloudFoundryAdminService adminService,
                                            TextProvider textProvider,
                                            TaskConfiguratorHelper taskConfiguratorHelper,
                                            EncryptionService encryptionService,
                                            CloudFoundryServiceFactory cloudFoundryServiceFactory) {
        this.adminService = adminService;
        this.textProvider = textProvider;
        this.taskConfiguratorHelper = taskConfiguratorHelper;
        this.encryptionService = encryptionService;
        this.cloudFoundryServiceFactory = cloudFoundryServiceFactory;
    }

    @Override
    @NotNull
    public Map<String, String> generateTaskConfigMap(@NotNull ActionParametersMap params,
            @Nullable TaskDefinition previousTaskDefinition) {
        Map<String, String> config = super.generateTaskConfigMap(params, previousTaskDefinition);
        taskConfiguratorHelper.populateTaskConfigMapWithActionParameters(config, params, FIELDS_TO_COPY);

        String passwordChange = params.getString(PASSWORD_CHANGE);
        if ("true".equals(passwordChange)) {
            String password = params.getString(ENVIRONMENT_PASSWORD);
            putPasswordInTaskConfigMap(password, config);
        } else if (previousTaskDefinition != null) {
            config.put(ENVIRONMENT_PASSWORD_ENCRYPTED, previousTaskDefinition.getConfiguration().get(ENVIRONMENT_PASSWORD_ENCRYPTED));
            config.put(ENVIRONMENT_IS_PASSWORD_ENCRYPTED, previousTaskDefinition.getConfiguration().get(ENVIRONMENT_IS_PASSWORD_ENCRYPTED));
        }else{
            String password = params.getString(ENVIRONMENT_PASSWORD);
            putPasswordInTaskConfigMap(password, config);
        }

        return config;
    }

    private void putPasswordInTaskConfigMap(String password, Map<String, String> config){
        if(containsBambooVariable(password)){
            config.put(ENVIRONMENT_PASSWORD_ENCRYPTED, password);
            config.put(ENVIRONMENT_IS_PASSWORD_ENCRYPTED, "false");
        } else {
            config.put(ENVIRONMENT_PASSWORD_ENCRYPTED, encryptionService.encrypt(password));
            config.put(ENVIRONMENT_IS_PASSWORD_ENCRYPTED, "true");
        }
    }

    /**
     * If the value of the field is blank, <code>null</code> is put to the
     * configMap. Otherwise the value is put to the configMap.
     * 
     * @param configMap
     * @param params
     * @param key
     */
    protected void putValueOrNull(Map<String, String> configMap, ActionParametersMap params, String key) {
        String value = params.getString(key);
        if (StringUtils.isBlank(value)) {
            configMap.put(key, null);
        } else {
            configMap.put(key, value);
        }
    }

    protected boolean containsBambooVariable(String value) {
        Matcher matcher = BAMBOO_VARIABLE_PATTERN.matcher(value);
        return matcher.matches();
    }

    @Override
    public void populateContextForCreate(@NotNull Map<String, Object> context) {
        super.populateContextForCreate(context);
        populateContextForAll(context);
    }

    @Override
    public void populateContextForEdit(@NotNull Map<String, Object> context, @NotNull TaskDefinition taskDefinition) {
        super.populateContextForEdit(context, taskDefinition);
        populateContextForAll(context);
        taskConfiguratorHelper.populateContextWithConfiguration(context, taskDefinition, FIELDS_TO_COPY);
    }

    @Override
    public void populateContextForView(@NotNull Map<String, Object> context, @NotNull TaskDefinition taskDefinition) {
        super.populateContextForView(context, taskDefinition);
        populateContextForAll(context);
        taskConfiguratorHelper.populateContextWithConfiguration(context, taskDefinition, FIELDS_TO_COPY);
    }

    private void populateContextForAll(@NotNull final Map<String, Object> context) {
        Map<String, String> targets = Maps.newLinkedHashMap();
        for (Target target : adminService.allTargets()) {
            targets.put(String.valueOf(target.getID()), target.getName());
        }
        context.put(TARGETS, targets);

        Map<String, String> buildTargets = Maps.newLinkedHashMap();
        for (Target target : adminService.allTargets()) {
            if(!target.isDisableForBuildPlans()){
                buildTargets.put(String.valueOf(target.getID()), target.getName());
            }
        }
        context.put(BUILD_PLAN_TARGETS, buildTargets);

        Map<String, String> targetConfigOptions = Maps.newHashMap();
        targetConfigOptions.put(ENVIRONMENT_CONFIG_OPTION_SHARED,
                textProvider.getText("cloudfoundry.task.global.environment.configOptions.shared"));
        targetConfigOptions.put(ENVIRONMENT_CONFIG_OPTION_TASK,
                textProvider.getText("cloudfoundry.task.global.environment.configOptions.task"));
        context.put(ENVIRONMENT_CONFIG_OPTIONS, targetConfigOptions);
    }

    @Override
    public void validate(@NotNull ActionParametersMap params, @NotNull ErrorCollection errorCollection) {
        super.validate(params, errorCollection);
        if(ENVIRONMENT_CONFIG_OPTION_SHARED.equals(params.getString(ENVIRONMENT_CONFIG_OPTION))) {
            validateRequiredNotBlank(TARGET_ID, params, errorCollection);
            validateRequiredNotBlank(ORGANIZATION, params, errorCollection);
            validateRequiredNotBlank(SPACE, params, errorCollection);
        }else if(ENVIRONMENT_CONFIG_OPTION_TASK.equals(params.getString(ENVIRONMENT_CONFIG_OPTION))){
            validateRequiredNotBlank(ENVIRONMENT_URL, params, errorCollection);
            validateRequiredNotBlank(ENVIRONMENT_USERNAME, params, errorCollection);

            if ("true".equals(params.getString(PASSWORD_CHANGE)) && noConnectionParametersContainVariables(params)) {
                validateRequiredNotBlank(ENVIRONMENT_PASSWORD, params, errorCollection);
                if (!errorCollection.hasAnyErrors()) {
                    validateCfConnection(params, errorCollection);
                }
            }
            // We are unable to validate the connection when the password hasn't changed because we don't have access
            // to the 'previousTaskDefinition' when doing validation
        } else {
            errorCollection.addError(ENVIRONMENT_CONFIG_OPTION, textProvider.getText("cloudfoundry.global.required.field"));
        }
    }

    private boolean noConnectionParametersContainVariables(ActionParametersMap params) {
        return !(anyParametersContainBambooVariables(params,
                ENVIRONMENT_URL,
                ENVIRONMENT_USERNAME,
                ENVIRONMENT_PASSWORD,
                ENVIRONMENT_PROXY_HOST,
                ENVIRONMENT_PROXY_PORT));
    }

    private boolean anyParametersContainBambooVariables(ActionParametersMap params, String... variables){
        for(String variable: variables){
            if(containsBambooVariable(params.getString(variable))){
                return true;
            }
        }
        return false;
    }

    protected void validateRequiredNotBlank(String field, ActionParametersMap params, ErrorCollection errorCollection) {
        if (StringUtils.isBlank(params.getString(field))) {
            errorCollection.addError(field, textProvider.getText("cloudfoundry.global.required.field"));
        }
    }

    private void validateCfConnection(ActionParametersMap params, ErrorCollection errorCollection) {
        ConnectionParameters.Builder connectionParameters = ConnectionParameters.builder()
                .targetUrl(params.getString(ENVIRONMENT_URL))
                .isTrustSelfSignedCerts(params.getBoolean(ENVIRONMENT_TRUST_CERTS))
                .username(params.getString(ENVIRONMENT_USERNAME))
                // The DefaultCloudFoundryServiceFactory expects passwords to be encrypted
                .password(encryptionService.encrypt(params.getString(ENVIRONMENT_PASSWORD)));

        if(StringUtils.isNotBlank(params.getString(ENVIRONMENT_IS_PASSWORD_ENCRYPTED))){
            connectionParameters.isPasswordEncrypted(Boolean.valueOf(params.getString(ENVIRONMENT_IS_PASSWORD_ENCRYPTED)));
        } else {
            connectionParameters.isPasswordEncrypted(true);
        }

        if (StringUtils.isNotBlank(params.getString(ENVIRONMENT_PROXY_HOST))) {
            connectionParameters.proxyHost(params.getString(ENVIRONMENT_PROXY_HOST));
            connectionParameters.proxyPort(params.getInt(ENVIRONMENT_PROXY_PORT, 80));
        }

        try {
            CloudFoundryService cloudFoundryService = cloudFoundryServiceFactory.getCloudFoundryService(connectionParameters.build(), new Log4jLogger());
        } catch (InvalidUrlException e) {
            errorCollection.addError(ENVIRONMENT_URL, textProvider.getText("cloudfoundry.task.global.environment.url.invalid"));
        } catch (CloudFoundryServiceException e) {
            errorCollection.addError(ENVIRONMENT_URL, textProvider.getText("cloudfoundry.task.global.environment.connectionDetails.invalid"));
        }
    }
}
