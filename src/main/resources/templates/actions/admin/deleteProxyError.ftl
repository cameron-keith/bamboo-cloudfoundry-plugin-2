<html>
<head>
	<meta name="decorator" content="adminpage">
</head>
<body>
	[@ui.header pageKey="cloudfoundry.admin.proxy.heading" /]
	<div class="aui-message error">
	    <p class="title">
	        <span class="aui-icon icon-error"></span>
	        <strong>[@ww.text name='cloudfoundry.admin.proxy.delete.error.message.header' /]</strong>
	    </p>
	    <p>[@ww.text name='cloudfoundry.admin.proxy.delete.error.message.description' /]</p>
	</div>
	[@ui.clear/]
	
	<p>[@ww.text name='cloudfoundry.admin.proxy.delete.error.details' /] <em>${name}</em>:</p>

	<table id="proxies" class="aui" width="100%">
		<thead><tr>
			<th class="labelPrefixCell">[@ww.text name='cloudfoundry.global.targets.list.heading.target' /]</th>
			<th class="operations">[@ww.text name='cloudfoundry.global.targets.list.heading.operations' /]</th>
		</tr></thead>
		<tbody>	
			[#foreach target in targetsUsingProxy]
				<tr>
					<td>
						${target.name}<br />
						[#if target.description?has_content]
							<span class="subGrey">${target.description}</span>
						[/#if]
					</td>
					<td>
						<a href="${req.contextPath}/admin/cloudfoundry/editTarget.action?targetId=${target.ID}">[@ww.text name='cloudfoundry.global.edit.target' /]</a>
					</td>
				</tr>			
			[/#foreach]
		</tbody>
	</table>
</body>
</html>