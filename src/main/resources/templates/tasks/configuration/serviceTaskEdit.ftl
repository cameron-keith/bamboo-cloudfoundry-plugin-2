[#include "selectTargetFragment.ftl"]

[@ui.bambooSection titleKey='cloudfoundry.task.service.section']

	[@ww.select labelKey="cloudfoundry.task.service.option" name="cf_option" list=cf_options listKey="key" listValue="value" toggle='true' /]
	[@ui.bambooSection dependsOn='cf_option' showOn='list']
		<p>[@ww.text name='cloudfoundry.task.service.list.description' /]</p>
	[/@ui.bambooSection]
	[@ui.bambooSection dependsOn='cf_option' showOn='show']
		<p>[@ww.text name='cloudfoundry.task.service.show.description' /]</p>
	    [@ww.textfield labelKey='cloudfoundry.task.service.showServiceName' name='cf_showServiceName' required='true' /]
	[/@ui.bambooSection]
	[@ui.bambooSection dependsOn='cf_option' showOn='create']
		<p>[@ww.text name='cloudfoundry.task.service.create.description' /]</p>
		[@ww.textfield labelKey='cloudfoundry.task.service.createServiceName' descriptionKey='cloudfoundry.task.service.createServiceName.description' name='cf_createLabel' required='true' /]
		[@ww.textfield labelKey='cloudfoundry.task.service.createServicePlan' descriptionKey='cloudfoundry.task.service.createServicePlan.description' name='cf_createPlan' required='true' /]
	    [@ww.textfield labelKey='cloudfoundry.task.service.createName' descriptionKey='cloudfoundry.task.service.createName.description' name='cf_createName' required='true' /]
		[@ww.checkbox labelKey='cloudfoundry.task.service.create.failTaskOnAlreadyExists' descriptionKey='cloudfoundry.task.service.create.failTaskOnAlreadyExists.description' name='cf_createFailIfExists' /]
	[/@ui.bambooSection]
	[@ui.bambooSection dependsOn='cf_option' showOn='delete']
		<p>[@ww.text name='cloudfoundry.task.service.delete.description' /]</p>
	    [@ww.textfield labelKey='cloudfoundry.task.service.deleteName' descriptionKey='cloudfoundry.task.service.deleteName.description' name='cf_deleteName' required='true' /]
	[/@ui.bambooSection]
	[@ui.bambooSection dependsOn='cf_option' showOn='bind']
		<p>[@ww.text name='cloudfoundry.task.service.bind.description' /]</p>
	    [@ww.textfield labelKey='cloudfoundry.task.service.bindServiceName' name='cf_bindServiceName' required='true' /]
	    [@ww.textfield labelKey='cloudfoundry.task.service.bindAppName' name='cf_bindAppName' required='true' /]
	[/@ui.bambooSection]
	[@ui.bambooSection dependsOn='cf_option' showOn='unbind']
		<p>[@ww.text name='cloudfoundry.task.service.unbind.description' /]</p>
	    [@ww.textfield labelKey='cloudfoundry.task.service.unbindServiceName' name='cf_unbindServiceName' required='true' /]
	    [@ww.textfield labelKey='cloudfoundry.task.service.unbindAppName' name='cf_unbindAppName' required='true' /]
	[/@ui.bambooSection]

[/@ui.bambooSection]

