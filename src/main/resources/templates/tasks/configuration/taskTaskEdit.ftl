[@ui.messageBox type="warning" titleKey="cloudfoundry.task.task.beta"]
    [@ww.text name="cloudfoundry.task.task.beta.message"]
    [/@ww.text]
[/@ui.messageBox]

[#include "selectTargetFragment.ftl"]

[@ui.bambooSection titleKey='cloudfoundry.task.task.section']

	[@ww.select labelKey="cloudfoundry.task.task.option" name="cf_option" list=cf_options listKey="key" listValue="value" toggle='true' /]
	[@ui.bambooSection dependsOn='cf_option' showOn='runTask']
		<p>[@ww.text name='cloudfoundry.task.task.runTask.description' /]</p>
		[@ww.textfield labelKey='cloudfoundry.task.global.applicationName' name='cf_runApplicationName' required='true' /]
		[@ww.textfield labelKey='cloudfoundry.task.task.command' name='cf_runCommand' required='true' cssClass="long-field" /]
		[@ww.textfield labelKey='cloudfoundry.task.task.name' name='cf_runTaskName' required='true' /]
		[@ww.textfield labelKey='cloudfoundry.task.task.memory' descriptionKey='cloudfoundry.task.task.memory.description' name='cf_runMemory' cssClass="short-field" /]
		[@ww.textarea labelKey='cloudfoundry.task.task.environment' descriptionKey='cloudfoundry.task.task.environment.description' name="cf_runEnvironment" /]
		[@ww.checkbox labelKey='cloudfoundry.task.task.captureTaskId' name='cf_runCaptureTaskId' toggle='true' /]
		[@ui.bambooSection dependsOn='cf_runCaptureTaskId' showOn='true']
			[@ww.textfield labelKey='cloudfoundry.task.task.taskIdVariableName' name='cf_runTaskIdVariableName' required='true' /]
		[/@ui.bambooSection]
	[/@ui.bambooSection]
    [@ui.bambooSection dependsOn='cf_option' showOn='waitOnTask']
        <p>[@ww.text name='cloudfoundry.task.task.waitOnTask.description' /]</p>
        [@ww.textfield labelKey='cloudfoundry.task.global.applicationName' name='cf_waitApplicationName' required='true' /]
        [@ww.textfield labelKey='cloudfoundry.task.task.id' name='cf_waitTaskId' required='true' /]
        [@ww.textfield labelKey='cloudfoundry.task.task.timeout' name='cf_waitTimeout' required='true' cssClass="short-field" /]
		[@ww.checkbox labelKey='cloudfoundry.task.task.cancelTaskOnTimeout' name='cf_waitTerminateTaskOnTimeout' /]
		[@ww.checkbox labelKey='cloudfoundry.task.task.failOnTaskFailure' name='cf_waitFailOnTaskFailure' /]
    [/@ui.bambooSection]

[/@ui.bambooSection]