package com.atlassian.bamboo.build.logger;

import com.atlassian.bamboo.Key;
import com.atlassian.bamboo.build.LogEntry;
import com.atlassian.bamboo.expirables.ExpiryTicker;
import com.atlassian.bamboo.utils.expirables.ExpiryTickerImpl;
import org.jetbrains.annotations.NotNull;

public class Log4jBuildLogger extends AbstractBuildLogger {

    private static final org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger("BambooBuildLog");

    public Log4jBuildLogger() {
        super("Log4jBuildLogger");
    }

    @Override
    public void onAddLogEntry(LogEntry logEntry) {
        LOG.info(logEntry.getLog());
    }

    @Override
    public void stopStreamingBuildLogs() {

    }

    @Override
    public boolean transfersLogsAsArtifact() {
        return false;
    }

    @Override
    public boolean isPersistent() {
        return false;
    }

    private static class StubbedKey implements Key {

        @Override
        public String getKey() {
            return "stub";
        }
    }
}
