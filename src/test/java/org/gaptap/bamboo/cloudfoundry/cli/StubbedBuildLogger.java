/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.cli;

import com.atlassian.bamboo.Key;
import com.atlassian.bamboo.build.LogEntry;
import com.atlassian.bamboo.build.logger.AbstractBuildLogger;
import com.atlassian.bamboo.build.logger.LoggerIds;
import com.atlassian.bamboo.utils.expirables.ExpiryTickerImpl;
import org.jetbrains.annotations.NotNull;

public class StubbedBuildLogger extends AbstractBuildLogger {

    public StubbedBuildLogger() {
        super("stubbedBuildLogger");
    }

    @Override
    public void onAddLogEntry(LogEntry logEntry) {

    }

    @Override
    public void stopStreamingBuildLogs() {

    }

    @Override
    public boolean transfersLogsAsArtifact() {
        return false;
    }

    @Override
    public boolean isPersistent() {
        return false;
    }

    private static class StubbedKey implements Key{

        @NotNull
        @Override
        public String getKey() {
            return "stub";
        }
    }
}
