package org.gaptap.bamboo.cloudfoundry.client;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import com.atlassian.bamboo.security.EncryptionService;

public class DefaultCloudFoundryServiceFactoryTest {

	@Mock
	private EncryptionService encryptionService;
	@Mock
	private ConnectionValidator validator;

	private DefaultCloudFoundryServiceFactory serviceFactory;
	private Logger logger = new Log4jLogger();

	@Before
	public final void setup() {
		MockitoAnnotations.initMocks(this);
		serviceFactory = new DefaultCloudFoundryServiceFactory(encryptionService);
		serviceFactory.setConnectionValidator(validator);
	}
	
	@Test
	public void getCloudFoundryService() {
		when(validator.isValid(any(CloudFoundryService.class))).thenReturn(true);
		ConnectionParameters.Builder connectionParametersBuilder = ConnectionParameters.builder()
		        .targetUrl("http://api.run.pivotal.io").username("tester").password("password")
		        .isPasswordEncrypted(false).isTrustSelfSignedCerts(true);

		serviceFactory.getCloudFoundryService(connectionParametersBuilder.build(), logger);
	}

	@Test(expected = InvalidUrlException.class)
	public void hasInvalidURL() {
		ConnectionParameters.Builder connectionParametersBuilder = ConnectionParameters.builder()
		        .targetUrl("api.run.pivotal.io").username("tester").password("password")
		        .isPasswordEncrypted(false).isTrustSelfSignedCerts(true);

		serviceFactory.getCloudFoundryService(connectionParametersBuilder.build(), logger);
	}

}
