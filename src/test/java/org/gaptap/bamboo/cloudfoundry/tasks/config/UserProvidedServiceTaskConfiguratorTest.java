package org.gaptap.bamboo.cloudfoundry.tasks.config;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.collections.SimpleActionParametersMap;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.webwork.util.ActionParametersMapImpl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.FileCopyUtils;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;

import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.BLUE_GREEN_ENABLED;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.UserProvidedServiceTaskConfigurator.DATA_OPTION_FILE;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.UserProvidedServiceTaskConfigurator.DATA_OPTION_INLINE;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.UserProvidedServiceTaskConfigurator.INLINE_DATA;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.UserProvidedServiceTaskConfigurator.REQUEST_FILE;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.UserProvidedServiceTaskConfigurator.ROUTE_SERVICE_URL;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.UserProvidedServiceTaskConfigurator.SELECTED_DATA_OPTION;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.UserProvidedServiceTaskConfigurator.SERVICE_NAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.UserProvidedServiceTaskConfigurator.SYSLOG_URL;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.RETURNS_SMART_NULLS;
import static org.mockito.Mockito.mock;

public class UserProvidedServiceTaskConfiguratorTest extends BaseCloudFoundryTaskConfiguratorTest {

    private UserProvidedServiceTaskConfigurator taskConfigurator;

    private Map<String, Object> params;

    @Before
    public void setup(){
        taskConfigurator = new UserProvidedServiceTaskConfigurator(adminService, textProvider, taskConfiguratorHelper, encryptionService, cloudFoundryServiceFactory);

        params = getBaseRequiredParameters();
    }

    private ActionParametersMap buildActionParametersMap(){
        return new ActionParametersMapImpl(params);
    }


    private String fileAsString(String fileName){
        String filename = "user-provided-services/" + fileName;
        ClassPathResource resource = new ClassPathResource(filename);
        try {
            return FileCopyUtils.copyToString(new InputStreamReader(resource.getInputStream()));
        } catch (IOException e) {
            throw new RuntimeException("Test setup error: unable to find test resources file with name " + filename, e);
        }

    }

    @Test
    public void allParametersAreSavedInTheConfigMap(){
        params.put(SERVICE_NAME, "test-service");
        params.put(SELECTED_DATA_OPTION, DATA_OPTION_INLINE);
        params.put(INLINE_DATA, fileAsString("simple-credentials.json"));
        params.put(REQUEST_FILE, "simple-credentials.json");
        params.put(SYSLOG_URL, "syslog://logs.example.com:1234");
        params.put(ROUTE_SERVICE_URL, "https://service.example.com");

        TaskDefinition previousTaskDefinition = mock(TaskDefinition.class, RETURNS_SMART_NULLS);
        Map<String, String> taskConfigMap = taskConfigurator.generateTaskConfigMap(buildActionParametersMap(), previousTaskDefinition);

        assertThat(taskConfigMap.get(SERVICE_NAME), is("test-service"));
        assertThat(taskConfigMap.get(SELECTED_DATA_OPTION), is(DATA_OPTION_INLINE));
        assertThat(taskConfigMap.get(INLINE_DATA), is(fileAsString("simple-credentials.json")));
        assertThat(taskConfigMap.get(REQUEST_FILE), is("simple-credentials.json"));
        assertThat(taskConfigMap.get(SYSLOG_URL), is("syslog://logs.example.com:1234"));
        assertThat(taskConfigMap.get(ROUTE_SERVICE_URL), is("https://service.example.com"));
    }

    @Test
    public void theServiceNameFieldAcceptsBambooVariables(){
        params.put(SERVICE_NAME, "${bamboo.serviceName}");
        params.put(SELECTED_DATA_OPTION, DATA_OPTION_INLINE);
        params.put(INLINE_DATA, fileAsString("simple-credentials.json"));

        assertTaskValidationHasNoErrors(taskConfigurator, buildActionParametersMap(), errorCollection);
    }

    @Test
    public void invalidInlineJsonResultsInAValidationError(){
        params.put(SERVICE_NAME, "test-service");
        params.put(SELECTED_DATA_OPTION, DATA_OPTION_INLINE);
        params.put(INLINE_DATA, fileAsString("invalid-credentials.json"));

        taskConfigurator.validate(buildActionParametersMap(), errorCollection);

        assertFalse(errorCollection.getErrors().isEmpty());
        assertNotNull(errorCollection.getErrors().get(INLINE_DATA));
    }

    @Test
    public void theInlineJsonDataAcceptsABambooVariableAsTheValue(){
        params.put(SERVICE_NAME, "test-service");
        params.put(SELECTED_DATA_OPTION, DATA_OPTION_INLINE);
        params.put(INLINE_DATA, "${bamboo.jsonContent}");

        assertTaskValidationHasNoErrors(taskConfigurator, buildActionParametersMap(), errorCollection);
    }

    @Test
    public void theInlineJsonDataAcceptsEmbededBambooVariables(){
        params.put(SERVICE_NAME, "test-service");
        params.put(SELECTED_DATA_OPTION, DATA_OPTION_INLINE);
        params.put(INLINE_DATA, fileAsString("credentials-with-variables.json"));

        assertTaskValidationHasNoErrors(taskConfigurator, buildActionParametersMap(), errorCollection);
    }

    @Test
    public void theJsonFileNameDataAcceptsABambooVariableAsTheValue(){
        params.put(SERVICE_NAME, "test-service");
        params.put(SELECTED_DATA_OPTION, DATA_OPTION_FILE);
        params.put(REQUEST_FILE, "${bamboo.fileName}");

        assertTaskValidationHasNoErrors(taskConfigurator, buildActionParametersMap(), errorCollection);
    }

    @Test
    public void theJsonFileNameDataAcceptsBambooVariableEmbeddedInTheValue(){
        params.put(SERVICE_NAME, "test-service");
        params.put(SELECTED_DATA_OPTION, DATA_OPTION_FILE);
        params.put(REQUEST_FILE, "environment-${bamboo.fileName}.json");

        assertTaskValidationHasNoErrors(taskConfigurator, buildActionParametersMap(), errorCollection);
    }
}
