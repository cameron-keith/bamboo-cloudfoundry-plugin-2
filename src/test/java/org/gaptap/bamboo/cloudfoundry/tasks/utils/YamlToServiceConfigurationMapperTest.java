/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.tasks.utils;

import org.gaptap.bamboo.cloudfoundry.client.ServiceConfiguration;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.is;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;

/**
 * @author David Ehringer
 */
public class YamlToServiceConfigurationMapperTest {

    private InputStream serviceManifest;
    private InputStream serviceManifestYamlParams;
    private InputStream serviceManifestMultipleServices;

    private YamlToServiceConfigurationMapper mapper = new YamlToServiceConfigurationMapper();

    @Before
    public void setup() throws FileNotFoundException {
        serviceManifest = new FileInputStream(new File("src/test/resources/service-manifest.yml"));
        serviceManifestYamlParams = new FileInputStream(new File("src/test/resources/service-manifest-params-yaml.yml"));
        serviceManifestMultipleServices = new FileInputStream(new File("src/test/resources/service-manifest-multiple-services.yml"));
    }

    @Test
    public void singleService(){
        List<ServiceConfiguration> services = mapper.from(serviceManifest);
        assertThat(services.size(), is(1));

        ServiceConfiguration serviceConfiguration = services.get(0);
        assertThat(serviceConfiguration.getService(), is("p-rabbitmq"));
        assertThat(serviceConfiguration.getServiceInstance(), is("my-service"));
        assertThat(serviceConfiguration.getPlan(), is("production"));

        assertThat(serviceConfiguration.getTags().size(), is(2));
        assertThat(serviceConfiguration.getTags().get(0), is("tag1"));
        assertThat(serviceConfiguration.getTags().get(1), is("tag2"));

        assertThat((String)serviceConfiguration.getParameters().get("some"), is("thing"));
        assertThat((String)serviceConfiguration.getParameters().get("something"), is("else"));
    }

    @Test
    public void jsonParameters(){
        List<ServiceConfiguration> services = mapper.from(serviceManifest);
        assertThat(services.size(), is(1));
        ServiceConfiguration serviceConfiguration = services.get(0);

        assertThat((String)serviceConfiguration.getParameters().get("some"), is("thing"));
        assertThat((String)serviceConfiguration.getParameters().get("something"), is("else"));
    }

    @Test
    public void yamlParameters(){
        List<ServiceConfiguration> services = mapper.from(serviceManifestYamlParams);
        assertThat(services.size(), is(1));
        ServiceConfiguration serviceConfiguration = services.get(0);

        assertThat((String)serviceConfiguration.getParameters().get("some"), is("other thing"));
        assertThat((String)serviceConfiguration.getParameters().get("something"), is("else"));
    }

    @Test
    public void multipleServices() {
        List<ServiceConfiguration> services = mapper.from(serviceManifestMultipleServices);
        assertThat(services.size(), is(2));
    }

    // TODO test cases
    // validation for required service, service_instance, plan
}
